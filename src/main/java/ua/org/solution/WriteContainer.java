package ua.org.solution;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;

/**
 * akka_test
 * Khoroshevskyi.M
 * 03.11.15  15:49
 */
public class WriteContainer {
private Map<Long, LongAdder> map;
private String outputFile;

  public WriteContainer(Map<Long, LongAdder> map, String outputFile) {
    this.map = map;
    this.outputFile = outputFile;
  }


  public Map<Long, LongAdder> getMap() {
    return map;
  }

  public String getOutputFile() {
    return outputFile;
  }
}

