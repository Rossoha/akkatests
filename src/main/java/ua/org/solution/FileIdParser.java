package ua.org.solution;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.japi.Pair;
import akka.pattern.Patterns;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.io.*;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;
import static akka.pattern.Patterns.ask;
import static akka.dispatch.Futures.fold;
import static akka.dispatch.Futures.future;

/**
 * akka_test
 * Khoroshevskyi.M
 * 03.11.15  12:13
 */
public class FileIdParser {

  public static final int TIMEOUT_MILLIS = 1000;
  private ActorSystem actorSystem = ActorSystem.create();
  private static final ConcurrentMap<Long, LongAdder> DICTIONARY = new ConcurrentHashMap<>();
  private ActorRef processor = actorSystem.actorOf(Props.create(Processor.class));

  public void parse(String inputFile , String outputFile) throws IOException {
    processor.tell(new ParseOperation(inputFile, outputFile),ActorRef.noSender());

  }

  }










