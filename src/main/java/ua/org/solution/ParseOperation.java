package ua.org.solution;

/**
 * akka_test
 * Khoroshevskyi.M
 * 03.11.15  14:16
 */
public class ParseOperation {
  private String outputFile;
  private String inputFile;

  public ParseOperation(String inputFile, String outputFile) {
    this.inputFile = inputFile;
    this.outputFile = outputFile;
  }

  public String getOutputFile() {
    return outputFile;
  }

  public String getInputFile() {
    return inputFile;
  }
}
