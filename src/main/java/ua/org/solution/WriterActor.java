package ua.org.solution;

import akka.actor.UntypedActor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.LongAdder;

/**
 * akka_test
 * Khoroshevskyi.M
 * 03.11.15  15:48
 */
public class WriterActor extends UntypedActor {
  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof WriteContainer) {
      WriteContainer container = (WriteContainer) message;


      Map<Long, LongAdder> map = container.getMap();


      FileWriter fileWriter = new FileWriter(container.getOutputFile(), false);
      BufferedWriter bufferWriter = new BufferedWriter(fileWriter);


      for (Entry<Long, LongAdder> entrie : map.entrySet()) {
        String result = entrie.getKey() + ":" + entrie.getValue().sum() + "\n";

        try {
          Files.write(Paths.get(container.getOutputFile()), result.getBytes(), StandardOpenOption.CREATE_NEW);

        } catch (IOException e) {
          //exception handling left as an exercise for the reader
        }



        System.out.printf("Close buffer");

      }


    }
  }
}
