package ua.org.solution;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.dispatch.OnSuccess;
import akka.japi.Pair;
import scala.concurrent.Future;

import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;

import static akka.dispatch.Futures.future;

/**
 * akka_test
 * Khoroshevskyi.M
 * 03.11.15  14:35
 */
public class Processor extends UntypedActor {

  ActorRef writer;

  @Override
  public void preStart() throws Exception {
    writer = context().actorOf(Props.create(WriterActor.class));
  }

  @Override
  public void onReceive(Object message) throws Exception {
    ConcurrentHashMap<Long, LongAdder> map = new ConcurrentHashMap<>();
    if (message instanceof ParseOperation) {
      ParseOperation parseOperation = (ParseOperation) message;

      BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(parseOperation.getInputFile())));
 in.lines().filter(Objects::nonNull).map(String::trim).forEach(amount -> future(() -> {

        String[] split = amount.split(":");
        Long id = Long.valueOf(split[0]);
        Long value = Long.valueOf(split[1]);
        return new Pair<Long, Long>(id, value);
      }, context().dispatcher()).onSuccess(new OnSuccess<Pair<Long, Long>>() {
        @Override
        public void onSuccess(Pair<Long, Long> pair) throws Throwable {

          if (!map.contains(pair.first())) {
            map.putIfAbsent(pair.first(), new LongAdder());
          }
          LongAdder adder = map.get(pair.first());
          adder.add(pair.second());
        }
      }, context().dispatcher()));






      System.out.println();
      writer.tell(new WriteContainer(map , parseOperation.getOutputFile()) , self());



    }


  }


}
